package com.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.entity.VEHICLE_BODY;

@Repository
public interface VehicleBodyRepository extends JpaRepository<VEHICLE_BODY,Integer>{

}
