package com.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.VEHICLE_ENGINE;
import com.entity.VEHICLE_TRANSACTION;


@Repository
public interface VehicleEngineRepository extends JpaRepository<VEHICLE_ENGINE,Integer>{

}
