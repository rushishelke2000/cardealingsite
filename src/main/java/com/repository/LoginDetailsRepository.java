package com.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.LOGIN_DETAILS;

@Repository
public interface LoginDetailsRepository extends JpaRepository<LOGIN_DETAILS, Integer> {

}
