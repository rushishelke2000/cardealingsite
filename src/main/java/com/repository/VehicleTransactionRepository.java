package com.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.VEHICLE_TRANSACTION;


@Repository
public interface VehicleTransactionRepository extends JpaRepository<VEHICLE_TRANSACTION,Integer>{

}
