package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.DEALER_DETAILS;

@Repository
public interface DealerDetailsRepository extends JpaRepository<DEALER_DETAILS, Integer>{

}
