package com.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class ROLES
{
	@Id
	private int _ROLEID;
	public int getROLEID()
	{
		return this._ROLEID;
	}
	public void setROLEID(int value)
	{
		this._ROLEID = value;
	}

	private String _ROLENAME;
	public String getROLENAME()
	{
		return this._ROLENAME;
	}
	public void setROLENAME(String value)
	{
		this._ROLENAME = value;
	}


	public ROLES(int ROLEID_,String ROLENAME_)
	{
		this._ROLEID = ROLEID_;
		this._ROLENAME = ROLENAME_;
	}
}
