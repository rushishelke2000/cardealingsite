package com.entity;
import java.sql.Timestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class VEHICLE_LINE
{
	@Id
	private int _LINE_ID;
	public int getLINE_ID()
	{
		return this._LINE_ID;
	}
	public void setLINE_ID(int value)
	{
		this._LINE_ID = value;
	}

	private String _LINE_NAME;
	public String getLINE_NAME()
	{
		return this._LINE_NAME;
	}
	public void setLINE_NAME(String value)
	{
		this._LINE_NAME = value;
	}

	@ManyToOne
	@JoinColumn(name="_MODEL_ID")
	private VEHICLE_MODEL _vehicle_model;
	
	@ManyToOne
	@JoinColumn(name="_ENGINE_ID")
	private VEHICLE_ENGINE _vehicle_engine;
	

	public VEHICLE_MODEL get_vehicle_model() {
		return _vehicle_model;
	}
	public void set_vehicle_model(VEHICLE_MODEL _vehicle_model) {
		this._vehicle_model = _vehicle_model;
	}
	public VEHICLE_ENGINE get_vehicle_engine() {
		return _vehicle_engine;
	}
	public void set_vehicle_engine(VEHICLE_ENGINE _vehicle_engine) {
		this._vehicle_engine = _vehicle_engine;
	}

	private int _PRICE;
	public int getPRICE()
	{
		return this._PRICE;
	}
	public void setPRICE(int value)
	{
		this._PRICE = value;
	}

	private String _LAST_UPDATED_BY;
	public String getLAST_UPDATED_BY()
	{
		return this._LAST_UPDATED_BY;
	}
	public void setLAST_UPDATED_BY(String value)
	{
		this._LAST_UPDATED_BY = value;
	}

	private java.sql.Timestamp _LAST_UPDATED_TIME;
	public java.sql.Timestamp getLAST_UPDATED_TIME()
	{
		return this._LAST_UPDATED_TIME;
	}
	public void setLAST_UPDATED_TIME(java.sql.Timestamp value)
	{
		this._LAST_UPDATED_TIME = value;
	}
	
	
	
	public VEHICLE_LINE() {
		super();
		// TODO Auto-generated constructor stub
	}
	public VEHICLE_LINE(int _LINE_ID, String _LINE_NAME, VEHICLE_MODEL _vehicle_model, VEHICLE_ENGINE _vehicle_engine,
			int _PRICE, String _LAST_UPDATED_BY, Timestamp _LAST_UPDATED_TIME) {
		super();
		this._LINE_ID = _LINE_ID;
		this._LINE_NAME = _LINE_NAME;
		this._vehicle_model = _vehicle_model;
		this._vehicle_engine = _vehicle_engine;
		this._PRICE = _PRICE;
		this._LAST_UPDATED_BY = _LAST_UPDATED_BY;
		this._LAST_UPDATED_TIME = _LAST_UPDATED_TIME;
	}
	
	
	

}
