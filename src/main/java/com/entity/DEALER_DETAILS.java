package com.entity;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class DEALER_DETAILS
{
	@Id
	private int _DEALERID;
	public int getDEALERID()
	{
		return this._DEALERID;
	}
	public void setDEALERID(int value)
	{
		this._DEALERID = value;
	}

	private String _DEALERNAME;
	public String getDEALERNAME()
	{
		return this._DEALERNAME;
	}
	public void setDEALERNAME(String value)
	{
		this._DEALERNAME = value;
	}

	private String _ADDRESS;
	public String getADDRESS()
	{
		return this._ADDRESS;
	}
	public void setADDRESS(String value)
	{
		this._ADDRESS = value;
	}

	private String _CONTACTNO;
	public String getCONTACTNO()
	{
		return this._CONTACTNO;
	}
	public void setCONTACTNO(String value)
	{
		this._CONTACTNO = value;
	}

	private String _EMAILID;
	public String getEMAILID()
	{
		return this._EMAILID;
	}
	public void setEMAILID(String value)
	{
		this._EMAILID = value;
	}

	private String _LASTUPDATEDBY;
	public String getLASTUPDATEDBY()
	{
		return this._LASTUPDATEDBY;
	}
	public void setLASTUPDATEDBY(String value)
	{
		this._LASTUPDATEDBY = value;
	}

	private java.sql.Timestamp _LASTUPDATEDTIME;
	public java.sql.Timestamp getLASTUPDATEDTIME()
	{
		return this._LASTUPDATEDTIME;
	}
	public void setLASTUPDATEDTIME(java.sql.Timestamp value)
	{
		this._LASTUPDATEDTIME = value;
	}


	public DEALER_DETAILS() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DEALER_DETAILS(int DEALERID_,String DEALERNAME_,String ADDRESS_,String CONTACTNO_,String EMAILID_,String LASTUPDATEDBY_,java.sql.Timestamp LASTUPDATEDTIME_)
	{
		this._DEALERID = DEALERID_;
		this._DEALERNAME = DEALERNAME_;
		this._ADDRESS = ADDRESS_;
		this._CONTACTNO = CONTACTNO_;
		this._EMAILID = EMAILID_;
		this._LASTUPDATEDBY = LASTUPDATEDBY_;
		this._LASTUPDATEDTIME = LASTUPDATEDTIME_;
	}
}