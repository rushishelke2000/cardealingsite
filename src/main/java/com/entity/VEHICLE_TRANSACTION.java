package com.entity;
import java.sql.Timestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

@Entity
public class VEHICLE_TRANSACTION
{
	@Id	
	private int _TRANSACTIONID;
	public int getTRANSACTIONID()
	{
		return this._TRANSACTIONID;
	}
	public void setTRANSACTIONID(int value)
	{
		this._TRANSACTIONID = value;
	}

	@OneToOne
	@JoinColumn(name="_CUSTID",referencedColumnName = "_CUSTID")
	public CUSTOMER_DETAILS _customer_details;
	
	@ManyToOne
	@JoinColumn(name="_LINE_ID")
	public VEHICLE_LINE _vehicle_line;
	
	@ManyToOne
	@JoinColumn(name="_MODEL_ID")
	public VEHICLE_MODEL _vehicle_model;
	
	
	@ManyToOne
	@JoinColumn(name="_BODY_ID")
	public VEHICLE_BODY _vehicle_body;
	
	@ManyToOne
	@JoinColumn(name="_ENGINE_ID")
	public VEHICLE_ENGINE _vehicle_engine;
	
	
	@ManyToOne
	@JoinColumn(name="_EQUIPMENT_ID")
	public VEHICLE_EQUIPMENT _vehicle_equipment;
	
	@ManyToOne
	@JoinColumn(name="INT_PAINT_ID")
	public VEHICLE_PAINT _vehicle_paint;
	
	@ManyToOne
	@JoinColumn(name="EXT_PAINT_ID")
	public VEHICLE_PAINT _vehicle_paintExt;
	
	
	private int _DISCOUNT;
	public int getDISCOUNT()
	{
		return this._DISCOUNT;
	}
	public void setDISCOUNT(int value)
	{
		this._DISCOUNT = value;
	}

	private int _TOTALPRICE;
	public int getTOTALPRICE()
	{
		return this._TOTALPRICE;
	}
	public void setTOTALPRICE(int value)
	{
		this._TOTALPRICE = value;
	}

	private String _LASTUPDATEDBY;
	public String getLASTUPDATEDBY()
	{
		return this._LASTUPDATEDBY;
	}
	public void setLASTUPDATEDBY(String value)
	{
		this._LASTUPDATEDBY = value;
	}

	private java.sql.Timestamp _LASTUPDATEDTIME;
	public java.sql.Timestamp getLASTUPDATEDTIME()
	{
		return this._LASTUPDATEDTIME;
	}
	public void setLASTUPDATEDTIME(java.sql.Timestamp value)
	{
		this._LASTUPDATEDTIME = value;
	}
	
	
	public VEHICLE_TRANSACTION(int _TRANSACTIONID, CUSTOMER_DETAILS _customer_details, VEHICLE_LINE _vehicle_line,
			VEHICLE_MODEL _vehicle_model, VEHICLE_BODY _vehicle_body, VEHICLE_ENGINE _vehicle_engine,
			VEHICLE_EQUIPMENT _vehicle_equipment, VEHICLE_PAINT _vehicle_paint, VEHICLE_PAINT _vehicle_paintExt,
			int _DISCOUNT, int _TOTALPRICE, String _LASTUPDATEDBY, Timestamp _LASTUPDATEDTIME) {
		super();
		this._TRANSACTIONID = _TRANSACTIONID;
		this._customer_details = _customer_details;
		this._vehicle_line = _vehicle_line;
		this._vehicle_model = _vehicle_model;
		this._vehicle_body = _vehicle_body;
		this._vehicle_engine = _vehicle_engine;
		this._vehicle_equipment = _vehicle_equipment;
		this._vehicle_paint = _vehicle_paint;
		this._vehicle_paintExt = _vehicle_paintExt;
		this._DISCOUNT = _DISCOUNT;
		this._TOTALPRICE = _TOTALPRICE;
		this._LASTUPDATEDBY = _LASTUPDATEDBY;
		this._LASTUPDATEDTIME = _LASTUPDATEDTIME;
	}
	
	
	
	
}