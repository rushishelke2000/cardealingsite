package com.entity;
import java.sql.Timestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class VEHICLE_EQUIPMENT
{
	@Id
	private int _EQUIPMENT_ID;
	public int getEQUIPMENT_ID()
	{
		return this._EQUIPMENT_ID;
	}
	public void setEQUIPMENT_ID(int value)
	{
		this._EQUIPMENT_ID = value;
	}

	private String _EQUIPMENT_NAME;
	public String getEQUIPMENT_NAME()
	{
		return this._EQUIPMENT_NAME;
	}
	public void setEQUIPMENT_NAME(String value)
	{
		this._EQUIPMENT_NAME = value;
	}
	
	
	@ManyToOne
	@JoinColumn(name="_MODEL_ID")
	private VEHICLE_MODEL _vehicle_model;
	
	@ManyToOne
	@JoinColumn(name="_ENGINE_ID")
	private VEHICLE_ENGINE _vehicle_engine;
	
	
	private int _PRICE;
	public int getPRICE()
	{
		return this._PRICE;
	}
	public void setPRICE(int value)
	{
		this._PRICE = value;
	}

	private String _LAST_UPDATED_BY;
	public String getLAST_UPDATED_BY()
	{
		return this._LAST_UPDATED_BY;
	}
	public void setLAST_UPDATED_BY(String value)
	{
		this._LAST_UPDATED_BY = value;
	}

	private java.sql.Timestamp _LAST_UPDATED_TIME;
	public java.sql.Timestamp getLAST_UPDATED_TIME()
	{
		return this._LAST_UPDATED_TIME;
	}
	public void setLAST_UPDATED_TIME(java.sql.Timestamp value)
	{
		this._LAST_UPDATED_TIME = value;
	}
	public VEHICLE_EQUIPMENT(int _EQUIPMENT_ID, String _EQUIPMENT_NAME, VEHICLE_MODEL _vehicle_model,
			VEHICLE_ENGINE _vehicle_engine, int _PRICE, String _LAST_UPDATED_BY, Timestamp _LAST_UPDATED_TIME) {
		super();
		this._EQUIPMENT_ID = _EQUIPMENT_ID;
		this._EQUIPMENT_NAME = _EQUIPMENT_NAME;
		this._vehicle_model = _vehicle_model;
		this._vehicle_engine = _vehicle_engine;
		this._PRICE = _PRICE;
		this._LAST_UPDATED_BY = _LAST_UPDATED_BY;
		this._LAST_UPDATED_TIME = _LAST_UPDATED_TIME;
	}
	
	
	
	



}