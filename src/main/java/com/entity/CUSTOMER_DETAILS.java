package com.entity;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class CUSTOMER_DETAILS
{
	@Id
	private int _CUSTID;
	public int getCUSTID()
	{
		return this._CUSTID;
	}
	public void setCUSTID(int value)
	{
		this._CUSTID = value;
	}

	private String _CUSTNAME;
	public String getCUSTNAME()
	{
		return this._CUSTNAME;
	}
	public void setCUSTNAME(String value)
	{
		this._CUSTNAME = value;
	}

	private String _MOBILENO;
	public String getMOBILENO()
	{
		return this._MOBILENO;
	}
	public void setMOBILENO(String value)
	{
		this._MOBILENO = value;
	}

	private String _EMAILID;
	public String getEMAILID()
	{
		return this._EMAILID;
	}
	public void setEMAILID(String value)
	{
		this._EMAILID = value;
	}

	private String _ADDRESS;
	public String getADDRESS()
	{
		return this._ADDRESS;
	}
	public void setADDRESS(String value)
	{
		this._ADDRESS = value;
	}

	private String _LASTUPDATEDBY;
	public String getLASTUPDATEDBY()
	{
		return this._LASTUPDATEDBY;
	}
	public void setLASTUPDATEDBY(String value)
	{
		this._LASTUPDATEDBY = value;
	}

	private java.sql.Timestamp _LASTUPDATEDTIME;
	public java.sql.Timestamp getLASTUPDATEDTIME()
	{
		return this._LASTUPDATEDTIME;
	}
	public void setLASTUPDATEDTIME(java.sql.Timestamp value)
	{
		this._LASTUPDATEDTIME = value;
	}


	public CUSTOMER_DETAILS(int CUSTID_,String CUSTNAME_,String MOBILENO_,String EMAILID_,String ADDRESS_,String LASTUPDATEDBY_,java.sql.Timestamp LASTUPDATEDTIME_)
	{
		this._CUSTID = CUSTID_;
		this._CUSTNAME = CUSTNAME_;
		this._MOBILENO = MOBILENO_;
		this._EMAILID = EMAILID_;
		this._ADDRESS = ADDRESS_;
		this._LASTUPDATEDBY = LASTUPDATEDBY_;
		this._LASTUPDATEDTIME = LASTUPDATEDTIME_;
	}
}