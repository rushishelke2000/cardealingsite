package com.entity;
import java.sql.Timestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class VEHICLE_PAINT
{
	@Id
	private int _PAINT_ID;
	public int getPAINT_ID()
	{
		return this._PAINT_ID;
	}
	public void setPAINT_ID(int value)
	{
		this._PAINT_ID = value;
	}

	private String _PAINT_NAME;
	public String getPAINT_NAME()
	{
		return this._PAINT_NAME;
	}
	public void setPAINT_NAME(String value)
	{
		this._PAINT_NAME = value;
	}

	private String _PAINT_TYPE;
	public String getPAINT_TYPE()
	{
		return this._PAINT_TYPE;
	}
	public void setPAINT_TYPE(String value)
	{
		this._PAINT_TYPE = value;
	}

	@ManyToOne
	@JoinColumn(name="_MODEL_ID")
	private VEHICLE_MODEL _vehicle_model;
	
	
	private int _PRICE;
	public int getPRICE()
	{
		return this._PRICE;
	}
	public void setPRICE(int value)
	{
		this._PRICE = value;
	}

	private String _LAST_UPDATED_BY;
	public String getLAST_UPDATED_BY()
	{
		return this._LAST_UPDATED_BY;
	}
	public void setLAST_UPDATED_BY(String value)
	{
		this._LAST_UPDATED_BY = value;
	}

	private java.sql.Timestamp _LAST_UPDATED_TIME;
	public java.sql.Timestamp getLAST_UPDATED_TIME()
	{
		return this._LAST_UPDATED_TIME;
	}
	public void setLAST_UPDATED_TIME(java.sql.Timestamp value)
	{
		this._LAST_UPDATED_TIME = value;
	}
	
	public VEHICLE_PAINT(int _PAINT_ID, String _PAINT_NAME, String _PAINT_TYPE, VEHICLE_MODEL _vehicle_model,
			int _PRICE, String _LAST_UPDATED_BY, Timestamp _LAST_UPDATED_TIME) {
		super();
		this._PAINT_ID = _PAINT_ID;
		this._PAINT_NAME = _PAINT_NAME;
		this._PAINT_TYPE = _PAINT_TYPE;
		this._vehicle_model = _vehicle_model;
		this._PRICE = _PRICE;
		this._LAST_UPDATED_BY = _LAST_UPDATED_BY;
		this._LAST_UPDATED_TIME = _LAST_UPDATED_TIME;
	}


}