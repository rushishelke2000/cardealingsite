package com.entity;
import java.sql.Timestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class VEHICLE_ENGINE
{
	@Id
	private int _ENGINE_ID;
	public int getENGINE_ID()
	{
		return this._ENGINE_ID;
	}
	public void setENGINE_ID(int value)
	{
		this._ENGINE_ID = value;
	}

	private String _ENGINE_NAME;
	public String getENGINE_NAME()
	{
		return this._ENGINE_NAME;
	}
	public void setENGINE_NAME(String value)
	{
		this._ENGINE_NAME = value;
	}

	@ManyToOne
	@JoinColumn(name="_MODEL_ID")
	private VEHICLE_MODEL _vehicle_model;
	

	public VEHICLE_MODEL getVehicle_model() {
		return _vehicle_model;
	}
	public void setVehicle_model(VEHICLE_MODEL vehicle_model) {
		this._vehicle_model = vehicle_model;
	}

	private int _PRICE;
	public int getPRICE()
	{
		return this._PRICE;
	}
	public void setPRICE(int value)
	{
		this._PRICE = value;
	}

	private String _LAST_UPDATED_BY;
	public String getLAST_UPDATED_BY()
	{
		return this._LAST_UPDATED_BY;
	}
	public void setLAST_UPDATED_BY(String value)
	{
		this._LAST_UPDATED_BY = value;
	}

	private java.sql.Timestamp _LAST_UPDATED_TIME;
	public java.sql.Timestamp getLAST_UPDATED_TIME()
	{
		return this._LAST_UPDATED_TIME;
	}
	public void setLAST_UPDATED_TIME(java.sql.Timestamp value)
	{
		this._LAST_UPDATED_TIME = value;
	}
	public VEHICLE_ENGINE(int _ENGINE_ID, String _ENGINE_NAME, VEHICLE_MODEL _vehicle_model, int _PRICE,
			String _LAST_UPDATED_BY, Timestamp _LAST_UPDATED_TIME) {
		super();
		this._ENGINE_ID = _ENGINE_ID;
		this._ENGINE_NAME = _ENGINE_NAME;
		this._vehicle_model = _vehicle_model;
		this._PRICE = _PRICE;
		this._LAST_UPDATED_BY = _LAST_UPDATED_BY;
		this._LAST_UPDATED_TIME = _LAST_UPDATED_TIME;
	}
	public VEHICLE_ENGINE() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	


}