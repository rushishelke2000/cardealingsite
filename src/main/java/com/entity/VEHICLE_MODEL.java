package com.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class VEHICLE_MODEL
{
	@Id
	private int _MODEL_ID;
	public int getMODEL_ID()
	{
		return this._MODEL_ID;
	}
	public void setMODEL_ID(int value)
	{
		this._MODEL_ID = value;
	}

	private String _MODEL_NAME;
	public String getMODEL_NAME()
	{
		return this._MODEL_NAME;
	}
	public void setMODEL_NAME(String value)
	{
		this._MODEL_NAME = value;
	}

	private String _LAST_UPDATED_BY;
	public String getLAST_UPDATED_BY()
	{
		return this._LAST_UPDATED_BY;
	}
	public void setLAST_UPDATED_BY(String value)
	{
		this._LAST_UPDATED_BY = value;
	}

	private java.sql.Timestamp _LAST_UPDATED_TIME;
	public java.sql.Timestamp getLAST_UPDATED_TIME()
	{
		return this._LAST_UPDATED_TIME;
	}
	public void setLAST_UPDATED_TIME(java.sql.Timestamp value)
	{
		this._LAST_UPDATED_TIME = value;
	}


	public VEHICLE_MODEL(int MODEL_ID_,String MODEL_NAME_,String LAST_UPDATED_BY_,java.sql.Timestamp LAST_UPDATED_TIME_)
	{
		this._MODEL_ID = MODEL_ID_;
		this._MODEL_NAME = MODEL_NAME_;
		this._LAST_UPDATED_BY = LAST_UPDATED_BY_;
		this._LAST_UPDATED_TIME = LAST_UPDATED_TIME_;
	}
	public VEHICLE_MODEL() {
		super();
		// TODO Auto-generated constructor stub
	}
}