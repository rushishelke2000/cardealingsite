package com.entity;
import java.sql.Timestamp;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class VEHICLE_BODY
{
	@Id
	private int _BODY_ID;
	public int getBODY_ID()
	{
		return this._BODY_ID;
	}
	public void setBODY_ID(int value)
	{
		this._BODY_ID = value;
	}

	private String _BODY_TYPE;
	public String getBODY_TYPE()
	{
		return this._BODY_TYPE;
	}
	public void setBODY_TYPE(String value)
	{
		this._BODY_TYPE = value;
	}
	
	@ManyToOne
	@JoinColumn(name="_MODEL_ID")
	private VEHICLE_MODEL _vehicle_model;
	

	public VEHICLE_MODEL getVehicle_model() {
		return _vehicle_model;
	}
	public void setVehicle_model(VEHICLE_MODEL vehicle_model) {
		this._vehicle_model = vehicle_model;
	}

	private String _LAST_UPDATED_BY;
	public String getLAST_UPDATED_BY()
	{
		return this._LAST_UPDATED_BY;
	}
	public void setLAST_UPDATED_BY(String value)
	{
		this._LAST_UPDATED_BY = value;
	}

	private java.sql.Timestamp _LAST_UPDATED_TIME;
	public java.sql.Timestamp getLAST_UPDATED_TIME()
	{
		return this._LAST_UPDATED_TIME;
	}
	public void setLAST_UPDATED_TIME(java.sql.Timestamp value)
	{
		this._LAST_UPDATED_TIME = value;
	}
	public VEHICLE_BODY(int _BODY_ID, String _BODY_TYPE, VEHICLE_MODEL vehicle_model, String _LAST_UPDATED_BY,
			Timestamp _LAST_UPDATED_TIME) {
		super();
		this._BODY_ID = _BODY_ID;
		this._BODY_TYPE = _BODY_TYPE;
		this._vehicle_model = vehicle_model;
		this._LAST_UPDATED_BY = _LAST_UPDATED_BY;
		this._LAST_UPDATED_TIME = _LAST_UPDATED_TIME;
	}
	public VEHICLE_BODY() {
		super();
		// TODO Auto-generated constructor stub
	}


}