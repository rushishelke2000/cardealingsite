package com.entity;
import java.sql.Timestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;

@Entity
public class LOGIN_DETAILS
{
	
	public LOGIN_DETAILS() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	private int _USERID;
	public int getUSERID()
	{
		return this._USERID;
	}
	public void setUSERID(int value)
	{
		this._USERID = value;
	}

	private String _USERNAME;
	public String getUSERNAME()
	{
		return this._USERNAME;
	}
	public void setUSERNAME(String value)
	{
		this._USERNAME = value;
	}

	private String _PWD;
	public String getPWD()
	{
		return this._PWD;
	}
	public void setPWD(String value)
	{
		this._PWD = value;
	}
	
	@OneToOne
	@JoinColumn(name="_DEALERID_FK")
	DEALER_DETAILS dealerDetails;
	
	

	public DEALER_DETAILS getDealerDetails() {
		return dealerDetails;
	}
	public void setDealerDetails(DEALER_DETAILS dealerDetails) {
		this.dealerDetails = dealerDetails;
	}

	private String _LASTUPDATEDBY;
	public String getLASTUPDATEDBY()
	{
		return this._LASTUPDATEDBY;
	}
	public void setLASTUPDATEDBY(String value)
	{
		this._LASTUPDATEDBY = value;
	}
	
	private java.sql.Timestamp _LASTUPDATEDTIME;
	public java.sql.Timestamp getLASTUPDATEDTIME()
	{
		return this._LASTUPDATEDTIME;
	}
	public void setLASTUPDATEDTIME(java.sql.Timestamp value)
	{
		this._LASTUPDATEDTIME = value;
	}
	public LOGIN_DETAILS(int _USERID, String _USERNAME, String _PWD, DEALER_DETAILS dealerDetails,
			String _LASTUPDATEDBY, Timestamp _LASTUPDATEDTIME) {
		super();
		this._USERID = _USERID;
		this._USERNAME = _USERNAME;
		this._PWD = _PWD;
		this.dealerDetails = dealerDetails;
		this._LASTUPDATEDBY = _LASTUPDATEDBY;
		this._LASTUPDATEDTIME = _LASTUPDATEDTIME;
	}



}