package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.VEHICLE_BODY;
import com.entity.VEHICLE_ENGINE;
import com.entity.VEHICLE_LINE;
import com.entity.VEHICLE_TRANSACTION;
import com.service.IVehicleInformationService;

@RestController
@RequestMapping("/vehicle")
public class VehicleController {
	
	@Autowired
	IVehicleInformationService vehicleService;
	
	@GetMapping("/body/{modelId}")
	public ResponseEntity<List<VEHICLE_BODY>> bodyByModel(@PathVariable int modelId)
	{
		return new ResponseEntity<List<VEHICLE_BODY>>(vehicleService.bodyByModel(modelId),HttpStatus.OK);
	}
	
	@GetMapping("/engine/{modelId}")
	public ResponseEntity<List<VEHICLE_ENGINE>> engineByModel(@PathVariable int modelId)
	{
		return new ResponseEntity<List<VEHICLE_ENGINE>>(vehicleService.engineByModel(modelId),HttpStatus.OK);
	}
	
	@GetMapping("/line/model={modelId}")
	public ResponseEntity<List<VEHICLE_LINE>> lineByModel(@PathVariable int modelId)
	{
		return new ResponseEntity<List<VEHICLE_LINE>>(vehicleService.lineByModel(modelId),HttpStatus.OK);
	}
	
	@GetMapping("/line/engine={engineId}")
	public ResponseEntity<List<VEHICLE_LINE>> lineByEngine(@PathVariable int engineId)
	{
		return new ResponseEntity<List<VEHICLE_LINE>>(vehicleService.lineByEngine(engineId),HttpStatus.OK);
	}
	
	@GetMapping("/line/model={modelId}/engine={engineId}")
	public ResponseEntity<List<VEHICLE_LINE>> lineByEngine(@PathVariable("modelId") int modelId,
			@PathVariable("engineId") int engineId)
	{
		return new ResponseEntity<List<VEHICLE_LINE>>(vehicleService.lineByModelAndEngine(modelId, engineId),
				HttpStatus.OK);
	}
	
	
}
