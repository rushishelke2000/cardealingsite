package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.LOGIN_DETAILS;
import com.service.ILoginDetailsService;

@RestController
@RequestMapping("/dienwagen")
public class LoginController {
	
	@Autowired
	ILoginDetailsService loginDetailsSerice;
	
	@GetMapping("/getLoginDetails/{lId}")
	public LOGIN_DETAILS getLoginDetails(@PathVariable int lId)
	{
		return loginDetailsSerice.showLoginDetails(lId);
	}
	
	

}
