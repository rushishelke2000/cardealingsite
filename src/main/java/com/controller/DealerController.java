package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.DEALER_DETAILS;
import com.service.IDealerDetailsService;


@RestController
@RequestMapping("/dealer")
public class DealerController {
	
	
	@Autowired
	IDealerDetailsService dealerDetailsService;
	
	
	
	@GetMapping("/searchDealer/{dealerId}/{pwd}")
	public ResponseEntity<?> searchDealer(@PathVariable("dealerId") int dealerId,@PathVariable("pwd") String pwd)
	{
		if((dealerDetailsService.searchDealer(dealerId, pwd)!=null)){
			
			return new ResponseEntity<DEALER_DETAILS>(dealerDetailsService.searchDealer(dealerId, pwd),HttpStatus.OK);
		}
		return new ResponseEntity<String>("Dealer not found",HttpStatus.BAD_REQUEST);
		

		
				
		
	}

	
	
	

}
