package com.service;

import com.entity.DEALER_DETAILS;

public interface IDealerDetailsService {
	
	public DEALER_DETAILS searchDealer(int dealerId, String pwd);

}
