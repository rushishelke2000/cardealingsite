package com.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.mapping.JpaMetamodelMappingContext;
import org.springframework.stereotype.Service;

import com.entity.DEALER_DETAILS;
import com.entity.LOGIN_DETAILS;
import com.repository.DealerDetailsRepository;
import com.repository.LoginDetailsRepository;

@Service
public class DealerDetailsServiceImpl implements IDealerDetailsService{
	
	@Autowired
	LoginDetailsRepository ldRepo; 
	
	@Autowired
	DealerDetailsRepository ddRepo;
	
	
		
//	com.dto.DEALER_DETAILS ddDTO = new com.dto.DEALER_DETAILS();
	
	
	
	@Override
	public DEALER_DETAILS searchDealer(int dealerId, String pwd) {
		for(LOGIN_DETAILS ld:ldRepo.findAll())
		{
			if(dealerId==ld.getDealerDetails().getDEALERID() && pwd.equals(ld.getPWD()))
			{
//				ddDTO.setDEALERID(ddRepo.findById(dealerId).get().getDEALERID());
//				ddDTO.setDEALERNAME(ddRepo.findById(dealerId).get().getDEALERNAME());
				
				
				
				Optional c = ddRepo.findById(dealerId);
				DEALER_DETAILS d1 = (DEALER_DETAILS) c.get();
//				return ddRepo.findById(dealerId).orElseThrow(null);
				return d1;
			}
		}
	return null;
	}

}
