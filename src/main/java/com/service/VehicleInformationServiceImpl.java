package com.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.entity.VEHICLE_BODY;
import com.entity.VEHICLE_ENGINE;
import com.entity.VEHICLE_EQUIPMENT;
import com.entity.VEHICLE_LINE;
import com.entity.VEHICLE_MODEL;
import com.entity.VEHICLE_PAINT;
import com.entity.VEHICLE_TRANSACTION;
import com.repository.VehicleBodyRepository;
import com.repository.VehicleEngineRepository;
import com.repository.VehicleLineRepository;
import com.repository.VehicleTransactionRepository;

@Service
public class VehicleInformationServiceImpl implements IVehicleInformationService{
	
	@Autowired
	VehicleTransactionRepository vtRepo;
	
	VEHICLE_TRANSACTION vehicleTransaction;
	
	@Autowired
	VehicleBodyRepository vbRepo;
	
	@Autowired
	VehicleEngineRepository veRepo;
	
	@Autowired
	VehicleLineRepository vlRepo;
	
	
	

	@Override
	public VEHICLE_TRANSACTION selectVehicleConfiguration(VEHICLE_BODY vb, VEHICLE_ENGINE ve,
			VEHICLE_EQUIPMENT veq,
			VEHICLE_LINE vl, VEHICLE_MODEL vm, VEHICLE_PAINT vp) {
		
		vehicleTransaction._vehicle_body.setBODY_ID(vb.getBODY_ID());
		vehicleTransaction._vehicle_line.setLINE_ID(vl.getLINE_ID());
		vehicleTransaction._vehicle_equipment.setEQUIPMENT_ID(veq.getEQUIPMENT_ID());
		vehicleTransaction._vehicle_model.setMODEL_ID(vm.getMODEL_ID());
		vehicleTransaction._vehicle_paint.setPAINT_ID(vp.getPAINT_ID());
		vehicleTransaction._vehicle_paintExt.setPAINT_ID(vp.getPAINT_ID());
		
		vtRepo.save(vehicleTransaction);
		return vehicleTransaction;
	}


	@Override
	public List<VEHICLE_BODY> bodyByModel(int modelId) {
		List<VEHICLE_BODY> vbList = new ArrayList<VEHICLE_BODY>();
		for(VEHICLE_BODY vb:vbRepo.findAll())
		{
			if(modelId==vb.getVehicle_model().getMODEL_ID())
			{
				vbList.add(vb);
			}
		}
		return vbList;
	}


	@Override
	public List<VEHICLE_ENGINE> engineByModel(int modelId) {
		List<VEHICLE_ENGINE> veList = new ArrayList<VEHICLE_ENGINE>();
		for(VEHICLE_ENGINE ve:veRepo.findAll())
		{
			if(modelId==ve.getVehicle_model().getMODEL_ID())
			{
				veList.add(ve);
			}
		}
		return veList;
	}


	@Override
	public List<VEHICLE_LINE> lineByModel(int modelId) {
		List<VEHICLE_LINE> vlList = new ArrayList<VEHICLE_LINE>();
		for(VEHICLE_LINE vl: vlRepo.findAll())
		{
			if(modelId == vl.get_vehicle_model().getMODEL_ID())
			{
				vlList.add(vl);
			}
		}
		return vlList;
	}


	@Override
	public List<VEHICLE_LINE> lineByEngine(int engineId) {
		List<VEHICLE_LINE> vlList = new ArrayList<VEHICLE_LINE>();
		for(VEHICLE_LINE vl: vlRepo.findAll())
		{
			if(engineId == vl.get_vehicle_engine().getENGINE_ID())
			{
				vlList.add(vl);
			}
		}
		
		
		return vlList;
	}


	@Override
	public List<VEHICLE_LINE> lineByModelAndEngine(int modelId, int engineId) {
		List<VEHICLE_LINE> vlList = new ArrayList<VEHICLE_LINE>();
		for(VEHICLE_LINE vl: vlRepo.findAll())
		{
			if((engineId == vl.get_vehicle_engine().getENGINE_ID()) && 
					(modelId == vl.get_vehicle_model().getMODEL_ID()))
			{
				vlList.add(vl);
			}
		}
		
		
		return vlList;
	}




}
