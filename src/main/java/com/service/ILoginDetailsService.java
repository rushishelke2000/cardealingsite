package com.service;

import java.util.List;

import com.entity.LOGIN_DETAILS;

public interface ILoginDetailsService {
	
	public LOGIN_DETAILS addLoginDetails(LOGIN_DETAILS loginDetails);
	
	public String removeLoginDetails(int userId);
	
	public LOGIN_DETAILS updateLoginDetails(int userId);
	
	public LOGIN_DETAILS showLoginDetails(int userId); 
	
	public List<LOGIN_DETAILS> showAllLoginDetails(); 
	
	
}
