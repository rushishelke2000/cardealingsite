package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.LOGIN_DETAILS;
import com.repository.LoginDetailsRepository;

@Service
public class LoginDetailsServiceImpl implements ILoginDetailsService{
	
	@Autowired
	LoginDetailsRepository ldRepo;

	@Override
	public LOGIN_DETAILS addLoginDetails(LOGIN_DETAILS loginDetails) {
		ldRepo.save(loginDetails);
		return loginDetails;
	}

	@Override
	public String removeLoginDetails(int userId) {
		ldRepo.deleteById(userId);
		return "Login Details removed";
	}

	@Override
	public LOGIN_DETAILS updateLoginDetails(int userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LOGIN_DETAILS showLoginDetails(int userId) {
		return (LOGIN_DETAILS)ldRepo.findById(userId).get();
	}

	@Override
	public List<LOGIN_DETAILS> showAllLoginDetails() {
		return ldRepo.findAll();
	}

}
