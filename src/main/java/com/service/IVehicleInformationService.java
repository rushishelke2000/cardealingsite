package com.service;

import java.util.List;

import com.entity.VEHICLE_BODY;
import com.entity.VEHICLE_ENGINE;
import com.entity.VEHICLE_EQUIPMENT;
import com.entity.VEHICLE_LINE;
import com.entity.VEHICLE_MODEL;
import com.entity.VEHICLE_PAINT;
import com.entity.VEHICLE_TRANSACTION;

public interface IVehicleInformationService {
	
//	public VEHICLE_BODY selectVehicleBody(int bodyId);
//	
//	public VEHICLE_ENGINE selectVehicleEngine();
//	
//	public VEHICLE_EQUIPMENT selectVehicleEquipment();
//
//	public VEHICLE_LINE selectVehicleLine();
//	
//	public VEHICLE_MODEL selectVehicleModel();
//	
//	public VEHICLE_PAINT selectVehicleColor();
	
	public VEHICLE_TRANSACTION selectVehicleConfiguration(
			VEHICLE_BODY vb,VEHICLE_ENGINE ve, VEHICLE_EQUIPMENT veq,
			VEHICLE_LINE vl,VEHICLE_MODEL vm, VEHICLE_PAINT vp
			);
	
	public List<VEHICLE_BODY> bodyByModel(int modelId);
	public List<VEHICLE_ENGINE> engineByModel(int modelId);
	public List<VEHICLE_LINE> lineByModel(int modelId);
	public List<VEHICLE_LINE> lineByEngine(int engineId);
	public List<VEHICLE_LINE> lineByModelAndEngine(int modelId, int engineId);
	
	
	

}
